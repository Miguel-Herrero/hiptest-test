# Hiptest test
0. Create scenarios on Hiptest, with action words
0. Pull those tests into the code with `hiptest-publisher`
0. Implement the tests *in the actionwords.js* and commit only that file.
0. Add a dedicated Test Run for CI
0. Let CI pull the results with `hiptest-publisher`

## Running it
```
npm install
npm test
```